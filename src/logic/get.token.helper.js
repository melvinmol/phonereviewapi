const { logger } = require('../config/config')

module.exports = {
    getToken: async (req, next) => {
        logger.info('Called get.token.helper in Logic')
        if (await !req.headers.authorization) {
            next({
                message: 'Authentication header missing.',
                code: 417
            })
        } else {
            return await req.headers.authorization
        }
    }
}