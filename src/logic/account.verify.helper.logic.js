const { logger, privateKey } = require('../config/config')
const Account = require('../datalayer/models/account.model')
const { mongoose } = require('mongoose')
const TokenHelper = require('./get.token.helper')
const jwt = require('jsonwebtoken')

module.exports = {
    /**
     * Verify account
     * Gets account by Email in the database
     * Checks if database got a result
     * Checks if password in database match with given password
     * Returns: account from database!!
     */
    verify: async (account) => {
        logger.info('Verify called in logic.')

        const dbAccount = await Account.findOne({ email: account.email })

        if (dbAccount !== null && dbAccount.password === account.password) {
            return true
        } else {
            return false
        }
    },
    /**
    * Validate token admin
    * Checks if has payload
    * Return: true
    */
    validateToken: async (req, next) => {
        logger.info('Called validateToken')

        const token = await TokenHelper.getToken(req, next)
        // Verifies the token
        const payload = await jwt.verify(token, privateKey)
        if (payload) {
            // Checks if the user has a UserId in the payload
            if (payload._id) {
                const account = await Account.findOne({ _id: payload._id })
                if (account && account.role >= 2) {
                    return true
                } else {
                    next({
                        message: 'Not authorized.',
                        code: 401
                    })
                }
            } else {
                next({
                    message: 'Id is missing.',
                    code: 401
                })
            }
        } else {
            next({
                message: 'Not authorized.',
                code: 401
            })
        }
    },

    /**
    * Validate token reviewer or higher
    * Checks if has payload
    * Return: true
    */
    validateTokenReviewer: async (req, next) => {
        logger.info('Called validateToken')

        const token = await TokenHelper.getToken(req, next)
        // Verifies the token
        const payload = await jwt.verify(token, privateKey)
        if (payload) {
            // Checks if the user has a UserId in the payload
            if (payload._id) {
                const account = await Account.findOne({ _id: payload._id })
                if (account && account.role >= 1) {
                    return true
                } else {
                    next({
                        message: 'Not authorized.',
                        code: 401
                    })
                }
            } else {
                next({
                    message: 'Id is missing.',
                    code: 401
                })
            }
        } else {
            next({
                message: 'Not authorized.',
                code: 401
            })
        }
    }
}