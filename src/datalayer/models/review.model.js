const mongoose = require('mongoose')
const Schema = mongoose.Schema
const ConOrProSchema = require('./con-or-pro-schema')

const ReviewSchema = new Schema({
    title: {
        type: String,
        required: [true, 'A Review should have a title.']
    },
    description: {
        type: String,
        required: [true, 'A Review should have a description.']
    },
    rating: {
        type: Number,
        required: [true, 'A Review should have a rating.']
    },
    consOrPros: {
        type: [ConOrProSchema],
        default: []
    }
})

const Review = mongoose.model('review', ReviewSchema)

module.exports = Review
