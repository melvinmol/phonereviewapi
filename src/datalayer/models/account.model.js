const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AccountSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A Account should have a name.']
    },
    email: {
        type: String,
        required: [true, 'A Account should have a email.']
    },
    role: {
        type: Number,
        required: [true, 'A Account should have a rol.']
    },
    password: {
        type: String,
        required: [true, 'A Account should have a password.']
    }
})

const Account = mongoose.model('account', AccountSchema)

module.exports = Account
