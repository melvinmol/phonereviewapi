const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ConOrProSchema = new Schema({
    conOrPro: {
        type: Boolean,
        required: [true, 'A ProOrCon should have a pro or a con.']
    },
    description: {
        type: String,
        required: [true, 'A ProOrCon should have a description.']
    }

})

module.exports = ConOrProSchema