const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ComponentSchema = new Schema({
    type: {
        type: String,
        required: [true, 'A Compoenent should have a type.']
    },
    description: {
        type: String,
        required: [true, 'A Component should have a description.']
    }
})

const Component = mongoose.model('component', ComponentSchema)

module.exports = Component
