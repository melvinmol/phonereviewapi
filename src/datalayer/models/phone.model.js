const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PhoneSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A Phone should have a name.']
    },
    company: {
        type: String,
        required: [true, 'A Phone should have a company.']
    },
    price: {
        type: Number,
        required: [true, 'A Phone should have a price.']
    },
    components: [{
        type: Schema.Types.ObjectId,
        ref: 'component',
        default: []
    }],
    reviews: [{
        type: Schema.Types.ObjectId,
        ref: 'review',
        default: []
    }]

})

const Phone = mongoose.model('phone', PhoneSchema)

module.exports = Phone
