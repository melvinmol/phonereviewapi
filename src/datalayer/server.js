const mongoose = require('mongoose')
const logger = require('../config/config').logger
const Account = require('./models/account.model')

const app = require('../../app')

if (process.env.NODE_ENV === 'testCloud' || process.env.NODE_ENV === 'production') {
    mongoose
        .connect('mongodb+srv://phone-review-user:YBrqVaQWZzYaYmyoKWnofvvjq@nick-melvin-studdit-txyf9.azure.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true })
        .then(() => {
            logger.info('MongoDB connection established')
            // fire the event that the app is ready to listen
            app.emit('databaseConnected')
        })
        .catch(err => {
            logger.error('MongoDB connection failed')
            logger.error(err)
        })
} else if (process.env.NODE_ENV !== 'test') {
    mongoose
        .connect('mongodb://localhost/phone-review', { useNewUrlParser: true })
        .then(() => {
            logger.info('MongoDB local connection established')
            // fire the event that the app is ready to listen
            app.emit('databaseConnected')
        })
        .catch(err => {
            logger.error('MongoDB local connection failed')
            logger.error(err)
        })
}

// since app inherits from Event Emitter, we can use this to get the app started
// after the database is connected
app.on('databaseConnected', function () {
    const port = process.env.PORT || 3500

    app.listen(port, () => {
        logger.info(`server is listening on port ${port}`)
    })
})
