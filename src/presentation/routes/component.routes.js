const express = require('express')
const router = express.Router()
const componentController = require('../controllers/component.controller')

router.post('/components', componentController.create)
router.get('/components', componentController.read)
router.get('/components/:componentId', componentController.readById)
router.put('/components/:componentId', componentController.update)
router.delete('/components/:componentId', componentController.delete)

module.exports = router
