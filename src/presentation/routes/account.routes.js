const express = require('express')
const router = express.Router()
const accountController = require('../controllers/account.controller')

router.post('/login', accountController.login)
router.post('/account', accountController.create)

module.exports = router
