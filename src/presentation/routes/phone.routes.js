const express = require('express')
const router = express.Router()
const phoneController = require('../controllers/phone.controller')

router.post('/phones/:phoneId/reviews', phoneController.createReview)
router.post('/phones', phoneController.create)
router.get('/phones', phoneController.read)
router.get('/phones/:phoneId', phoneController.readById)
router.get('/phones/:phoneId/reviews', phoneController.readReviewsByPhoneId)
router.put('/phones/:phoneId', phoneController.update)
router.delete('/phones/:phoneId', phoneController.delete)

module.exports = router