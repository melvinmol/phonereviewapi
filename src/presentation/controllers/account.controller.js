const { logger, privateKey } = require('../../config/config')
const Account = require('../../datalayer/models/account.model')
const jwt = require('jsonwebtoken')
const Verify = require('../../logic/account.verify.helper.logic')

/**
 * Account Controller
 */
module.exports = {
    /**
     * Login the account
     * Checks if body has properties email and password
     * Verify the account
     * Res: token
     */
    login: async (req, res, next) => {
        logger.info('Login called in controller')

        try {
            if (req.body && req.body.password && req.body.email) {
                if (await Verify.verify(req.body)) {

                    const account = await Account.findOne({ email: req.body.email }, { password: 0 })
                    const token = jwt.sign({ _id: account._id }, privateKey)
                    res.status(200).json({
                        account: account,
                        token: token
                    })
                } else {
                    next({
                        message: 'Email or password is incorrect.',
                        code: 401
                    })
                }
            } else {
                next({
                    message: 'A email and password are required.',
                    code: 400
                })
            }
        } catch (err) {
            logger.error(err)

            // Optimize by creating function
            next({
                message: 'Something went wrong by creating new Account.',
                code: 204
            })
        }
    },

    create: async (req, res, next) => {
        logger.info('Create called in Account controller')
        try {
            const result = await Verify.validateToken(req, next)
            if (result) {
                const account = new Account(req.body)
                await account.save()
                res.status(200).send({ _id: account._id })
            }
        } catch (err) {
            logger.error(err)

            // Optimize by creating function
            next({
                message: 'Something went wrong by creating new Account.',
                code: 204
            })
        }
    }
}