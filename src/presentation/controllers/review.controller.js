const { logger, privateKey } = require('../../config/config')
const Review = require('../../datalayer/models/phone.model')
const jwt = require('jsonwebtoken')
const Verify = require('../../logic/account.verify.helper.logic')

module.exports = {
    create: async (req, res, next) => {
        logger.info('Create called in Phone controller')
        try {
            const result = await Verify.validateToken(req, next)
            if (result) {
                const phone = new Phone(req.body)
                await phone.save()
                res.status(200).send({ _id: phone._id })
            }
        } catch (err) {
            logger.error(err)

            // Optimize by creating function
            next({
                message: 'Something went wrong by creating new Phone.',
                code: 204
            })
        }
    },

    read: async (req, res, next) => {
        logger.info('Read called in Phone controller')
        try {
            const phones = await Phone.find({}).populate('components')
            res.status(200).send({ result: phones })
        } catch (err) {
            res.status(400).end()
        }
    },

    readById: async (req, res, next) => {
        logger.info('Read by id called in Phone controller')
        try {
            const id = req.params.phoneId
            const phone = await Phone
                .findOne({ _id: id })
                .populate('components')
                .populate('reviews')
                .populate('prosAndCons')
            res.status(200).send({ result: phone })
        } catch (err) {
            res.status(400).end()
        }
    },

    update: async (req, res, next) => {
        logger.info('Update called in Phone controller')
        try {
            const result = await Verify.validateToken(req, next)
            if (result) {
                id = req.params.phoneId
                logger.info(req.body)
                await Phone.updateOne({ _id: id }, {
                    name: req.body.name,
                    company: req.body.company,
                    price: req.body.price,
                    components: req.body.components
                })
                res.status(200).send({ _id: id })
            }
        } catch (err) {
            logger.error(err)

            next({
                message: 'Something went wrong by updating a Phone.',
                code: 400
            })
        }
    },

    delete: async (req, res, next) => {
        logger.info('Delete called in Phone controller')
        try {
            const result = await Verify.validateToken(req, next)
            if (result) {
                const id = req.params.phoneId
                await Phone.findByIdAndRemove({ _id: id })
                res.status(200).send({})
            }
        } catch (err) {
            logger.error(err)

            next({
                message: 'Something went wrong by deleting a Phone.',
                code: 400
            })
        }
    }
}