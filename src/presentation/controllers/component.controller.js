const { logger, privateKey } = require('../../config/config')
const Component = require('../../datalayer/models/component.model')
const jwt = require('jsonwebtoken')
const Verify = require('../../logic/account.verify.helper.logic')

/**
 * Account Controller
 */
module.exports = {
    create: async (req, res, next) => {
        logger.info('Create called in Component controller')
        try {
            const result = await Verify.validateToken(req, next)
            if (result) {
                const component = new Component(req.body)
                await component.save()
                res.status(200).send({ _id: component._id })
            }
        } catch (err) {
            logger.error(err)

            // Optimize by creating function
            next({
                message: 'Something went wrong by creating new Component.',
                code: 204
            })
        }
    },

    read: async (req, res, next) => {
        logger.info('Read called in Component controller')
        try {
            const components = await Component.find({})
            res.status(200).send({ result: components })
        } catch (err) {
            res.status(400).end()
        }
    },

    readById: async (req, res, next) => {
        logger.info('Read called in Component controller')
        try {
            const id = req.params.componentId
            const component = await Component.findOne({ _id: id })
            res.status(200).send({ result: component })
        } catch (err) {
            res.status(400).end()
        }
    },

    update: async (req, res, next) => {
        logger.info('Update called in Component controller')
        try {
            const result = await Verify.validateToken(req, next)
            if (result) {
                id = req.params.componentId
                logger.info(req.body)
                await Component.updateOne({ _id: id }, {
                    type: req.body.type,
                    description: req.body.description
                })
                res.status(200).send({ _id: id })
            }
        } catch (err) {
            logger.error(err)

            next({
                message: 'Something went wrong by updating a Component.',
                code: 400
            })
        }
    },

    delete: async (req, res, next) => {
        logger.info('Delete called in Component controller')
        try {
            const result = await Verify.validateToken(req, next)
            if (result) {
                const id = req.params.componentId
                await Component.findByIdAndRemove({ _id: id })
                res.status(200).send({})
            }
        } catch (err) {
            logger.error(err)

            next({
                message: 'Something went wrong by deleting a Component.',
                code: 400
            })
        }
    }
}