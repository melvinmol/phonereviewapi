const express = require('express')
var cors = require('cors')
const app = express()
const logger = require('./src/config/config').logger

// parse body of incoming request
const bodyParser = require('body-parser')

app.use(cors())
app.use(bodyParser.json())

const accountRoutes = require('./src/presentation/routes/account.routes')
const componentRoutes = require('./src/presentation/routes/component.routes')
const phoneRoutes = require('./src/presentation/routes/phone.routes')

app.use('/api', accountRoutes)
app.use('/api', componentRoutes)
app.use('/api', phoneRoutes)

/**
 * Handle endpoint not found
 */
app.all('*', (req, res, next) => {
    const { method, url } = req
    const errorMessage = `${method} ${url} does not exist.`
    logger.warn(errorMessage)
    const errorObject = {
        message: errorMessage,
        code: 404,
        date: new Date()
    }
    next(errorObject)
})

/**
 * Error handler
 */
app.use((error, req, res, next) => {
    logger.error('Error handler: ', error.message.toString())
    res.status(error.code).json(error)
})

// export the app object for use elsewhere
module.exports = app
